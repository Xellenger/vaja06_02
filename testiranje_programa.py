#!/usr/bin/env python
"""Program je namenjen testiranju gitlab CI"""

import numpy as np

def konv_frekvenca_mono(signal, impulz):
    """Funkcija za konvolucijo - en kanal"""
    signal = signal / np.max(np.abs(signal))
    impulz = impulz / np.max(np.abs(impulz))
    dolzina_signal = signal.shape[0]
    signal = np.pad(signal, ((0, impulz.shape[0] - 1), (0,0)), mode='constant')
    impulz = np.pad(impulz, ((0, dolzina_signal - 1), (0,0)), mode='constant')
    x_1 = np.fft.fft2(signal)
    y_1 = np.fft.fft2(impulz)
    efekt = x_1 * y_1
    efekt = np.fft.ifft2(efekt)
    return efekt / np.max(np.abs(efekt))

def konv_frekvenca_stereo(signal, impulz):
    """Funkcija za konvolucijo - dva kanala"""
    signal = signal / np.max(np.abs(signal))
    impulz = impulz / np.max(np.abs(impulz))
    dolzina_signal = signal.shape[0]
    signal = np.pad(signal, ((0, impulz.shape[0] - 1), (0,0)), mode='constant')
    impulz = np.pad(impulz, ((0, dolzina_signal - 1), (0,0)), mode='constant')
    x_1 = np.fft.fft2(signal[:,0].reshape(-1,1))
    y_1 = np.fft.fft2(impulz[:,0].reshape(-1,1))
    efekt1 = x_1 * y_1
    efekt1 = np.fft.ifft2(efekt1)
    x_2 = np.fft.fft2(signal[:,1].reshape(-1,1))
    y_2 = np.fft.fft2(impulz[:,1].reshape(-1,1))
    efekt2 = x_2 * y_2
    efekt2 = np.fft.ifft2(efekt2)
    efekt = np.concatenate((efekt1, efekt2),axis=1)
    return efekt / np.max(np.abs(efekt))

if __name__ == '__main__':
    signalMono = np.array([1,3,2,8,2,5,2,5,1,2]).reshape(-1,1)
    signalStereo = np.array([[1,3,2,8,2,5,2,5,1,2],[5,8,2,8,6,2,6,3,8,2]]).T
    impulzMono = np.array([6,2,4]).reshape(-1,1)
    impulzStereo = np.array([[6,2,4], [2,5,9]]).T
    efektMono = konv_frekvenca_mono(signalMono,impulzMono)
    efektStereo = konv_frekvenca_stereo(signalStereo, impulzStereo)
    outputFileMono = open("monoOutput.txt", "w")
    outputFileMono.write(np.array2string(efektMono))
    outputFileMono.close()
    outputFileStereo = open("stereoOutput.txt", "w")
    outputFileStereo.write(np.array2string(efektStereo))
    outputFileStereo.close()
